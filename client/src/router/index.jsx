import { Route, Routes } from "react-router-dom";
import { NewsCreateUpdate } from "../pages/NewsCreateUpdate";
import { Article } from "../pages/Article";
import { News } from "../pages/News";

export default function Router() {
	return (
		<Routes>
			<Route
				path="/"
				element={<News />}
			/>
			<Route
				path="/news/:newsId"
				element={<Article />}
			/>
			<Route
				path="/newsCreateUpdate"
				element={<NewsCreateUpdate />}
			/>
			<Route
				path="/newsCreateUpdate/:articleId"
				element={<NewsCreateUpdate />}
			/>
		</Routes>
	);
}
